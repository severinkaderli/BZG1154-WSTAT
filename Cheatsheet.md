---
title: "Cheatsheet"
---
# Processes
## Direction Execution
* Program gets full control in kernel mode

## Limited Direction Execution
* Interupts to switch back to kernel mode

## Context Switch
* Timer Interrupt

## `fork`, `wait`, `exec`